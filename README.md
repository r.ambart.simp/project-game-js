# **Project-Game-JS**

Project-Game-JS est un jeu video du genre "shoot'em up" developper en pure javascript pour un projet d'étude. Developper en seulement 3 semaines , les fonctionalités principales d'un "shoot'em up" classic ont été implementées:

- System d'ennemies aléatoire.
- System de tire et de collision
- Un niveau complet jouable.

Je vous invites a tester cette premiere version.

## Comment jouer.

Commencez le jeu en appuyant sur la touche "Start Game".  Dirigez votre personnage avec les touches fleches de votre clavier et tirez avec la touche X. 

Accumulez 7000pts afin de faire apparaitre le Boss et enfin vainquez le afin de terminer le niveau.

 ## ScreenShot du jeu:

<img src = "screen1.png">

<img src = "screen2.png">

Technologies Utiliser.

- Javascript.

  

- CSS.

- HTML.

- GitLab.

## Fonctionement.

Le jeu est entierement baser sur des fonctions Javascript ainsi que sur des intervals de lancement.

Le principe est tres simple , une function vas venir crée des ennemies aléatoirement et les faire animer grace aux animations CSS. Exemple ci dessous:

```javascript
function getRandomPosition(element) {
    let x = document.body.offsetHeight - element.clientHeight;
    let y = document.body.offsetWidth - element.clientWidth;
    let randomX = Math.floor(Math.random() * x);
    let randomY = Math.floor(Math.random() * y);
    return [randomX, randomY];
}

function spawnEnemy() {
    let enemy = document.createElement('img');
    let playground = document.querySelector('.playground');
    playground.appendChild(enemy);
    
    enemy.setAttribute("src", "assets/sprite.png");
    enemy.classList.add('ennemis');
    enemy.id = 'ennemyalien';
    
    let xy = getRandomPosition(enemy);
    enemy.style.top = xy[0] + 'px';
    enemy.style.left = xy[0] + 'px';
}

```

Une autre function vas venir supprimer les ennemies crée precedement a chaque collision detecter. Nous allons demander a notre fonction de prendre les hauteurs ainsi que les largeurs afin de detecter les collisions entre deux elements et appliquer les actions que l'on veut. 

Ici , nous allons simplement supprimer les elements toucher.

 Exemples ci-dessous:

```javascript
function bulletCol(bullet) {
    let ennemi = document.querySelectorAll('.ennemis');

    for (let item of ennemi) {
        if (bullet.offsetLeft < item.offsetLeft + item.offsetWidth &&
            bullet.offsetLeft + bullet.offsetWidth > item.offsetLeft &&
            bullet.offsetTop < item.offsetTop + item.offsetHeight &&
            bullet.offsetHeight + bullet.offsetTop > item.offsetTop) {
            console.log('collision!');
            
             item.remove();
        }
    }

}
```

 

## Version.

Actuellement en version 0.9.7.

 Des améliorations et des nouvelles ajouts sont susceptible d'etre apporter prochainement comme:

- Plus d'optimisations et de fluidité.
- Nouveaux ennemies
- Plusieurs niveaux de jeux.

## Auteur.

@Rapha , formation Simplon.SE , promo9.

## **Contact**.

Pour plus d'information ou de simple questions n'hesitez pas a me contactez via mail : r.ambart.simp@gmail.com

Vous pouvez aussi me retrouvez sur Simplonlyon.fr.




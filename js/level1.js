let background = new Audio();
let audio = new Audio();
let explo = new Audio();

background.src = "assets/background.wav";
background.play();






let health = 100;
let score = 0;
let bosshealth = 2000;


//HUD

function hudCol() {

    let hud = document.querySelectorAll('.hud');

    let player = document.querySelector('.ship');


    for (let item of hud) {
        if (player.offsetLeft < item.offsetLeft + item.offsetWidth &&
            player.offsetLeft + player.offsetWidth > item.offsetLeft &&
            player.offsetTop < item.offsetTop + item.offsetHeight &&
            player.offsetHeight + player.offsetTop > item.offsetTop) {

            console.log('collision');
            item.classList.add('hudAnim');

        } else {
            item.classList.remove('hudAnim');
        }
    }
}

setInterval(function () {
    hudCol();
}, 100);




// KEYBOARD & MOOVEMENT


/// store key codes and currently pressed ones
let keys = {};
keys.UP = 38;
keys.LEFT = 37;
keys.RIGHT = 39;
keys.DOWN = 40;

/// store reference to character's position and element
let character = {
    x: 100,
    y: 100,
    speedMultiplier: 5,
    element: document.querySelector(".ship")
};

/// key detection (better to use addEventListener, but this will do)
document.body.onkeyup =
    document.body.onkeydown = function (e) {
        if (e.preventDefault) {
            e.preventDefault();
        }
        else {
            e.returnValue = false;
        }
        let kc = e.keyCode || e.which;
        keys[kc] = e.type == 'keydown';
    };

/// character movement update
let moveCharacter = function (dx, dy) {
    character.x += (dx || 0) * character.speedMultiplier;
    character.y += (dy || 0) * character.speedMultiplier;
    character.element.style.left = character.x + 'px';
    character.element.style.top = character.y + 'px';
};

let ship = document.querySelector('.ship');

/// character control
let detectCharacterMovement = function () {
    if (keys[keys.LEFT] && ship.offsetLeft >= 10) {
        moveCharacter(-2, 0);
    }
    if (keys[keys.RIGHT] && ship.offsetLeft <= 1275) {
        moveCharacter(2, 0);
    }
    if (keys[keys.UP] && ship.offsetTop >= 10) {
        moveCharacter(0, -2);
    }
    if (keys[keys.DOWN] && ship.offsetTop <= 550) {
        moveCharacter(0, 2);
    }
};

/// update current position on screen
moveCharacter();

/// game loop
setInterval(function () {
    detectCharacterMovement();
}, 1000 / 24);



document.querySelector('body').addEventListener("keyup", function (event) {

    if (event.code === 'KeyX') {

        bullets();
        audio.src = "assets/shoot.wav";
        audio.play();
        
    }

})



//end of mouvement//



//ENEMY SPAWN//

// random ennemy
function getRandomPosition(element) {

    let x = document.body.offsetHeight - element.clientHeight;
    let y = document.body.offsetWidth - element.clientWidth;
    let randomX = Math.floor(Math.random() * x);
    let randomY = Math.floor(Math.random() * y);
    return [randomX, randomY];
}
function spawnEnemy() {
    let enemy = document.createElement('img');
    let playground = document.querySelector('.playground');

    playground.appendChild(enemy);


    enemy.setAttribute("src", "assets/sprite.png");

    enemy.classList.add('ennemis');
    enemy.id = 'ennemyalien';



    let xy = getRandomPosition(enemy);
    enemy.style.top = xy[0] + 'px';
    enemy.style.left = xy[0] + 'px';

    // collision ennemi
    setInterval(function () {
        ennemyCol();
    }, 100)

    setTimeout(function () {
        enemy.remove();
    }, 6000)

}

function spawnEnemy2() {
    let enemy = document.createElement('img');
    let playground = document.querySelector('.playground');

    playground.appendChild(enemy);


    enemy.setAttribute("src", "assets/ennemiship.png");

    enemy.classList.add('ennemis', 'ennemyfire');
    enemy.id = 'ennemyship';



    let xy = getRandomPosition(enemy);
    enemy.style.top = xy[0] + 'px';
    enemy.style.left = xy[0] + 'px';

    // collision ennemi
    setInterval(function () {
        ennemyCol();
    }, 100)

    setTimeout(function () {
        enemy.remove();
    }, 10000)

}




function spawnBoss1() {
    let playground = document.querySelector('.playground');
    let enemyBoss = document.createElement('img');
    playground.appendChild(enemyBoss);


    enemyBoss.setAttribute("src", "assets/sprite1.png");

    enemyBoss.classList.add('enemyBoss1Bullets', 'bossMoov');
    enemyBoss.id = 'enemyBoss1';
}


function test() {
    if (score >= 7000) {
        return true;
    }
}




let interval = setInterval(function () {

    if (test() === true) {
        spawnBoss1();
        clearInterval(interval);
    }
}, 1000)





// collision ennemi
setInterval(function () {
    ennemyCol();
}, 100)





// player enemmis collision

function ennemyCol() {

    let ennemi = document.querySelectorAll('.ennemis');

    for (let item of ennemi) {
        if (ship.offsetLeft < item.offsetLeft + item.offsetWidth &&
            ship.offsetLeft + ship.offsetWidth > item.offsetLeft &&
            ship.offsetTop < item.offsetTop + item.offsetHeight &&
            ship.offsetHeight + ship.offsetTop > item.offsetTop) {

            console.log('collision');


            health = health - 1;
            let lblHealth = document.querySelector('.lblHealth');
            lblHealth.innerHTML = health;

            item.src = 'assets/explosion.gif';

            setTimeout(function () {
                item.remove();
            }, 200)


        }

    }

}




// setInterval(spawnEnemy, 1000);

function callFunctions(func, num, delay) {
    if (!num) return;
    func();
    setTimeout(function () { callFunctions(func, num - 1, delay); }, delay);
}
callFunctions(spawnEnemy, 20, 1500);
callFunctions(spawnEnemy2, 15, 2500);
callFunctions(spawnEnemy, 10, 3500);
callFunctions(spawnEnemy2, 10, 4500);




// Player bullets

function bullets() {

    let playground = document.querySelector(".playground")
    let player = document.querySelector(".ship");


    let bullet = document.createElement('img');
    bullet.classList.add('bullet')
    bullet.src = 'assets/bullet.png';


    bullet.style.top = (player.offsetTop + player.offsetHeight / 3) + "px";
    bullet.style.left = (player.offsetLeft + player.offsetLeft / 10) + "px";


    playground.appendChild(bullet);

    setTimeout(function () {
        bullet.remove();
    }, 1200)


    setInterval(function () {
        bulletCol(bullet);
    }, 100);


}



//player bullet collision
function bulletCol(bullet) {

    let ennemi = document.querySelectorAll('.ennemis');

    for (let item of ennemi) {


        if (bullet.offsetLeft < item.offsetLeft + item.offsetWidth &&
            bullet.offsetLeft + bullet.offsetWidth > item.offsetLeft &&
            bullet.offsetTop < item.offsetTop + item.offsetHeight &&
            bullet.offsetHeight + bullet.offsetTop > item.offsetTop) {
            console.log('hit!');
            score = score + 100;

            item.src = 'assets/explosion.gif';

            let lblScore = document.querySelector('.lblScore');
            lblScore.innerHTML = score;

            setTimeout(function () {
                item.remove();
            }, 100)

            explo.src = "assets/explosion.wav";
            explo.play();


            setTimeout(function () {
                bullet.remove();
            }, 100)
        }
    }

}







// Enemy bullets //

function enemyBullets() {


    let playground = document.querySelector(".playground");

    let enemy = document.querySelectorAll(".ennemyfire");
    let ennemyFire = enemy[Math.floor(Math.random() * enemy.length)];


    let bullet = document.createElement('div');
    bullet.classList.add('enemybullet')


    bullet.style.top = (ennemyFire.offsetTop + ennemyFire.offsetHeight / 3) + "px";
    bullet.style.left = (ennemyFire.offsetLeft + ennemyFire.offsetLeft / 15) + "px";


    playground.appendChild(bullet);



    setTimeout(function () {
        console.log(bullet);
        bullet.remove();
    }, 2500)

    setInterval(function () { EnemybulletCol(bullet); }, 100);

}


// enemy bullet collision
function EnemybulletCol(bullet) {

    let player = document.querySelectorAll('.ship');

    for (let item of player) {


        if (bullet.offsetLeft < item.offsetLeft + item.offsetWidth &&
            bullet.offsetLeft + bullet.offsetWidth > item.offsetLeft &&
            bullet.offsetTop < item.offsetTop + item.offsetHeight &&
            bullet.offsetHeight + bullet.offsetTop > item.offsetTop) {
            bullet.remove();


            health = health - 20;
            let lblHealth = document.querySelector('.lblHealth');
            lblHealth.innerHTML = health;

        }
    }

}

setInterval(function () {
    enemyBullets();
}, 1600)






function bossBullets() {


    let playground = document.querySelector(".playground");

    let enemy = document.querySelectorAll("#enemyBoss1");
    let ennemyFire = enemy[Math.floor(Math.random() * enemy.length)];


    let bullet = document.createElement('img');
    bullet.classList.add('enemyBoss1Bullets');
   bullet.setAttribute("src", "assets/alien3.png");

    let bullet2 = document.createElement('img');
    bullet2.classList.add('enemyBoss1Bullets');
   bullet2.setAttribute("src", "assets/alien3.png");

    let bullet3 = document.createElement('img');
    bullet3.classList.add('enemyBoss1Bullets');
    bullet3.setAttribute("src", "assets/alien3.png");


    bullet.style.top = (ennemyFire.offsetTop + ennemyFire.offsetHeight - 150) + "px";
    bullet.style.left = (ennemyFire.offsetLeft + ennemyFire.offsetLeft / 3) + "px";

    bullet2.style.top = (ennemyFire.offsetTop + ennemyFire.offsetHeight - 300) + "px";
    bullet2.style.left = (ennemyFire.offsetLeft + ennemyFire.offsetLeft / 3 ) + "px";


    bullet3.style.top = (ennemyFire.offsetTop + ennemyFire.offsetHeight  - 450) + "px";
    bullet3.style.left = (ennemyFire.offsetLeft + ennemyFire.offsetLeft / 3) + "px";



    playground.appendChild(bullet);
    playground.appendChild(bullet2);
    playground.appendChild(bullet3);
    
    setInterval(function () { bossBulletCol(bullet); }, 100);
    setInterval(function () { bossBulletCol(bullet2); }, 100);
    setInterval(function () { bossBulletCol(bullet3); }, 100);


    setTimeout(function () {
        console.log(bullet);
        bullet.remove();
    }, 2500)


    setTimeout(function () {
        console.log(bullet2);
        bullet2.remove();
    }, 2500)


    setTimeout(function () {
        console.log(bullet3);
        bullet3.remove();
    }, 2500)


}


setInterval(function () {
    bossBullets();
}, 1600)



//player vs boss collision

function bulletBossCol() {
  
    let bullet = document.querySelector('.bullet');

    let boss = document.querySelectorAll('#enemyBoss1');

    for (let item of boss) {


        if (bullet.offsetLeft < item.offsetLeft + item.offsetWidth &&
            bullet.offsetLeft + bullet.offsetWidth > item.offsetLeft &&
            bullet.offsetTop < item.offsetTop + item.offsetHeight &&
            bullet.offsetHeight + bullet.offsetTop > item.offsetTop) {
            console.log('hit!!!!');


            bosshealth = bosshealth - 10;
            let lblbossHealth = document.querySelector('.lblbossHealth');
            lblbossHealth.innerHTML = bosshealth;
            


            setInterval(function () { bullet.remove(); }, 1000);
        }
    }

}


setInterval(function () { bulletBossCol(); }, 100);



// enemy bullet collision
function bossBulletCol(bullet) {

    
    let player = document.querySelectorAll('.ship');

    for (let item of player) {


        if (bullet.offsetLeft < item.offsetLeft + item.offsetWidth &&
            bullet.offsetLeft + bullet.offsetWidth > item.offsetLeft &&
            bullet.offsetTop < item.offsetTop + item.offsetHeight &&
            bullet.offsetHeight+ bullet.offsetTop > item.offsetTop) {
            bullet.remove();


            health = health - 1;
            let lblHealth = document.querySelector('.lblHealth');
            lblHealth.innerHTML = health;


        }
    }

}


function bossBulletCol(bullet2) {

    let player = document.querySelectorAll('.ship');

    for (let item of player) {


        if (bullet2.offsetLeft < item.offsetLeft + item.offsetWidth &&
            bullet2.offsetLeft + bullet2.offsetWidth > item.offsetLeft &&
            bullet2.offsetTop < item.offsetTop + item.offsetHeight &&
            bullet2.offsetHeight+ bullet2.offsetTop > item.offsetTop) {
            bullet2.remove();


            health = health - 1;
            let lblHealth = document.querySelector('.lblHealth');
            lblHealth.innerHTML = health;


        }
    }

}

function bossBulletCol(bullet3) {

    let player = document.querySelectorAll('.ship');

    for (let item of player) {


        if (bullet3.offsetLeft < item.offsetLeft + item.offsetWidth &&
            bullet3.offsetLeft + bullet3.offsetWidth > item.offsetLeft &&
            bullet3.offsetTop < item.offsetTop + item.offsetHeight &&
            bullet3.offsetHeight+ bullet3.offsetTop > item.offsetTop) {
            bullet3.remove();


            health = health - 10;
            let lblHealth = document.querySelector('.lblHealth');
            lblHealth.innerHTML = health;


        }
    }

}








function playerDead() {
    let player = document.querySelector('.ship');

    if (health <= 0) {
        
        player.src = 'assets/explosion.gif';

     
        setInterval(function () { document.location.href="gameover.html" }, 1000);
        }
}

setInterval(function () {
    playerDead();
}, 100);



function playerWin() {
    let boss = document.querySelector('#enemyBoss1');

    if (bosshealth <= 0) {
        boss.src = 'assets/explosion.gif';
           
        setInterval(function () { document.location.href="youwin.html" }, 1000);
    }
}

setInterval(function () {
    playerWin();
}, 100);





